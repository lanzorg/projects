## <samp>UBUHOGEN</samp>

Opinionated configuration script for Ubuntu 22.10.

**One Command Execution**

```shell
wget -qO- https://raw.githubusercontent.com/sharpordie/ubuhogen/HEAD/ubuhogen.sh | bash
```

**Miscellaneous Screenshots**

<img src="https://fakeimg.pl/852x480/273445/fff/?text=‏‏‎ ‎" width="49.5%"/><img src="https://upload.wikimedia.org/wikipedia/commons/c/ca/1x1.png" width="1%"/><img src="https://fakeimg.pl/852x480/273445/fff/?text=‏‏‎ ‎" width="49.5%"/>
